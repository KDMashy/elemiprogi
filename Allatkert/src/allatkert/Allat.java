package allatkert;
public class Allat {
    protected Integer kor;
    protected String nev;

    public Allat(Integer kor, String nev) {
        this.kor = kor;
        this.nev = nev;
    }
    

    public Integer getKor() {
        return kor;
    }

    public String getNev() {
        return this.nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }
    
    public void szulinap(){
        this.kor++;
    }
    
    
    
    
    
}
